def is_square_number (n):
	while n > 0:
		digit = n % 10
		if digit in [6, 8, 9]:
			return True
		n = n / 10
	return False

n = int(input("Enter n: "))
a = 0
while n > 0:
	a = a +1
	if is_square_number (a):
		n = n - 1
print(a)
