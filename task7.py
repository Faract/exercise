n = int(input("Enter fib number: ")) #порядковый номер
fib1 = 1
fib2 = 1
i = 0 #само число
m = 2
if n == 0 or n == 1:
    i = 1 
else:
    while m <= n:
        i = fib1 + fib2
        fib1 = fib2
        fib2 = i 
        m += 1 
print(i)