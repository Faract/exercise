def task_1(a):
	return list(map(lambda x: x ** 2, a))

def task_2 (a,b,c):
	return list(map(lambda x,y,z: x * y * z, a,b,c))

def task_3 (d):
	return list(map(lambda x: len(x), d))

def task_4 (a):
		return list(filter(lambda x: x % 2 == 0, a))
		
def task_5 (d):
	return list(filter(lambda x: len(x) != 0, d))
	
def task_6 (a,b,c):
	return list(zip(a,b,c))
	
def task_7 (a,b):
		return list(zip(a,list(map(lambda x: x ** 2, b))))

a = [1, 2, 3, 4, 5]
b = [5, 4,3,2,1]
c = [11, 12, 13, 14, 15]
d = ['df','', 'asfasdfa','', 's', 'aaaa']

print(task_1(a))
print(task_2(a,b,c))
print(task_3(d))
print(task_4(a))
print(task_5(d))
print(task_6(a,b,c))
print(task_7(a,b))