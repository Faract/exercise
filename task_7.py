def new_zip(*args):
	return list(map(lambda *x: tuple(x), *args))
	
a = [1, 2, 3, 4, 5]
b = [5, 4, 3, 2, 1]
print(new_zip(a, b))